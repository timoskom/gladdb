#pragma once
#include "files.h"
#include "Column.h"
#include "EntryValue.h"

GLADDBBEGIN

std::stringstream create_table_wrapper(std::string name, std::string db, std::vector<Column> columns);

std::stringstream append_entry_to_table_wrapper(std::stringstream tablestream, std::vector<EntryValue> values);

std::optional<std::stringstream> remove_entry_from_table_wrapper(std::stringstream tablestream, std::vector<EntryValue> values);

GLADDBEND