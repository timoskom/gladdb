#pragma once
#include "config.h"
#include "columns_base.h"
#include "files.h"
#include "ColumnInfo.h"
#include "Column.h"
#include "EntryValue.h"

GLADDBBEGIN

class Table {
public:

	using Entry = std::vector<EntryValue>;

	Table(const std::string& name, const std::string& db, std::vector<Column> columns);

	Table(const Table&) = default;

	GLADDBINLINE virtual ~Table() { }

	Table& operator=(const Table&) = default;

	bool create();

	bool appendEntry(Entry values);

	bool removeEntry(Entry values);

private:

	std::fstream _stream;
	std::string _name;
	std::string _db;
	std::vector<Column> _columns;
	std::vector<Entry> _entries;

};

GLADDBEND