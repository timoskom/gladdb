#include "include/files.h"

using std::array;
using std::fstream;
using std::optional;
using std::pair;
using std::string;
using std::stringstream;
using std::vector;
using std::writeline;

using GLADDB ColumnNullability;
using GLADDB ColumnPropertiesType;
using GLADDB ColumnType;
using GLADDB OpenMode;
using GLADDB tfstream;

optional<fstream> GLADDB write_to_file(const string name, const string data, const OpenMode mode) {
	fstream flstream{ name, cast_enum(OpenMode::TRUNCATE) };
	if (!(flstream.fail() || flstream.bad())) {
		flstream << data;
		return flstream;
	}
	else {
		return std::nullopt;
	}
}

stringstream GLADDB create_table(string name, string db, vector<pair<string, pair<ColumnType, ColumnPropertiesType>>> columns) {
	stringstream strstream{ cast_enum(OpenMode::APPEND) };
	writeline(strstream, "file#TABLE");
	writeline(strstream, "TABLE[name=" + name + ", db=" + db + "]");
	{
		stringstream colstream{ "COLUMNS[", cast_enum(OpenMode::APPEND) };
		for (size_t i = 0u; i < columns.size(); i++) {
			colstream << columns[i].first << "=" << columns[i].second.first << ":0x";
			colstream << std::setfill('0') << std::setw(sizeof(ColumnPropertiesType) * 2u) << std::uppercase << std::hex << (int)columns[i].second.second;
			if (i + 1 < columns.size()) {
				colstream << ", ";
			}
		}
		colstream << "]";
		writeline(strstream, colstream.str());
	}
	return strstream;
}

stringstream GLADDB append_entry_to_table(stringstream tablestream, vector<pair<string, ColumnType>> values) {
	array<string, 3> arr{};
	stringstream strstream{ cast_enum(OpenMode::APPEND) };
	strstream.str(tablestream.str());

	strstream << "-[";
	for (size_t i = 0; i < values.size(); i++) {
		strstream << values[i].first;
		if (i + 1 < values.size()) {
			strstream << ", ";
		}
	}
	strstream << "]" << std::endl;

	return strstream;
}

optional<stringstream> GLADDB remove_entry_from_table(stringstream tablestream, vector<string> values) {
	string str = tablestream.str();
	stringstream bufstream{ cast_enum(OpenMode::APPEND) };

	bufstream << "-[";
	for (size_t i = 0; i < values.size(); i++) {
		bufstream << values[i];
		if (i + 1 < values.size()) {
			bufstream << ", ";
		}
	}
	bufstream << "]" << std::endl;
	string bufstr = bufstream.str();

	string::size_type pos = str.find(bufstr);
	if (pos != string::npos) {
		str.erase(pos, bufstr.size());
		stringstream retstream{ cast_enum(OpenMode::APPEND) };
		retstream.str(bufstr);
		return retstream;
	} else {
		return std::nullopt;
	}
}

optional<fstream> GLADDB create_master_table_file(string db) {
	stringstream strstream = GLADDB create_table("MASTER", db, vector<pair<string, pair<ColumnType, ColumnPropertiesType>>>{
		{
			std::make_pair("_id", std::make_pair(ColumnType::Integer, GLADDB create_column_properties(ColumnNullability::NotNull))),
				std::make_pair("tables", std::make_pair(ColumnType::Text, GLADDB create_column_properties(ColumnNullability::NotNull)))
		}
	});

	strstream.str(append_entry_to_table(stringstream{ strstream.str(), cast_enum(OpenMode::APPEND) }, vector<pair<string, ColumnType>>{
		{
			std::make_pair("1", ColumnType::Integer),
				std::make_pair("'MASTER'", ColumnType::Text)
		}
	}).str());

	return write_to_file("MASTERS.txt", strstream.str(), OpenMode::TRUNCATE);
}

optional<bool> GLADDB create_database_file(string db) {
	try {
		if (!std::filesystem::exists(db)) {
			using namespace bit7z;

			Bit7zLibrary lib{ GLADDB LIB7Z };
			BitFileCompressor compressor{ lib, BitFormat::Zip };

			vector<string> files = { "MASTER.txt" };

			// Creating a zip archive with a custom directory structure
			std::map<string, string > files_map = {
				{ ".ninja_log", "alias/.ninja_log" }
			};

			{
				auto optional = GLADDB create_master_table_file(db);
				if (optional.has_value()) {
					optional.value().close();
					writeline(std::cout, "all-good");
				}
				else {
					writeline(std::cout, "dsklds");
				}
			}

			compressor.compress(files, db);

			return true;
		}
		else {
			return false;
		}
	}
	catch (const bit7z::BitException& ex) {
		std::cout << ex.what() << std::endl;
		return false;
	}
}

bool GLADDB destroy_database_file(string db) {
	if (std::filesystem::exists(db)) {
		return (bool)std::filesystem::remove(db);
	}
	else {
		return false;
	}
}

optional<tfstream> GLADDB get_database_file(string db, bool binary_mode) {
	if (std::filesystem::exists(db)) {
		std::ios_base::openmode mode = cast_enum(OpenMode::APPEND);
		if (binary_mode) {
			mode |= std::ios_base::binary;
		}
		return tfstream{ db, mode };
	}
	else {
		return std::nullopt;
	}
}