﻿#include "gladDB.h"

using namespace std;

vector<string> parse_args(int argc, char** argv, bool with_program) {
	vector<string> args{};
	for (size_t i = ((with_program) ? 0u : 1u); i < (size_t)argc; i++) {
		args.push_back(argv[i]);
	}
	return args;
}

void handle_args(int argc, char** argv) {
	vector<string> args = parse_args(argc, argv, false);

	for (string arg : args) {

	}
}

int main(int argc, char** argv)
{
	int ret = EXIT_SUCCESS;
	handle_args(argc, argv);
	try {
		//std::fstream fl{"test.txt"};
		GLADDB create_database_file("glad.7z");
		std::filesystem::remove("glad.7z");
	} catch (const std::exception& ex) {
		std::cout << ex.what() << std::endl;
	}
	std::cin.get();
	return ret;
}
