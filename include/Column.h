#pragma once

#include "config.h"
#include "columns_base.h"
#include "ColumnInfo.h"

GLADDBBEGIN

class Column {
public:

	GLADDBINLINE
		Column(const std::string& name, const ColumnInfo& info)
		:_name{ name }, _info{ info }
	{}

	Column(const Column&) = default;

	GLADDBINLINE virtual ~Column() { }

	Column& operator=(const Column&) = default;

	GLADDBINLINE
		std::string name() const {
		return this->_name;
	}

	GLADDBINLINE
		ColumnInfo info() const {
		return this->_info;
	}


private:

	std::string _name;
	ColumnInfo _info;

};

GLADDBEND