#include "include/files_table_ext.h"

using std::optional;
using std::pair;
using std::string;
using std::stringstream;
using std::vector;

using GLADDB Column;
using GLADDB ColumnInfo;
using GLADDB ColumnPropertiesType;
using GLADDB ColumnType;
using GLADDB EntryValue;

namespace {

	pair<string, pair<ColumnType, ColumnPropertiesType>> convert_column(Column column) {
		const string name = column.name();
		const ColumnInfo info = column.info();
		return std::make_pair(
			name,
			std::make_pair(
				info.type(),
				info.properties()
			)
		);
	}

	pair<string, ColumnType> convert_entry_value(EntryValue value) {
		return std::make_pair(
			value.value(),
			value.type()
		);
	}

}

stringstream GLADDB create_table_wrapper(string name, string db, vector<Column> columns) {
	vector<pair<string, pair<ColumnType, ColumnPropertiesType>>> _converted_columns;
	for (const auto column : columns) {
		const string name = column.name();
		const ColumnInfo info = column.info();
		_converted_columns.push_back(::convert_column(column));
	}
	return create_table(name, db, _converted_columns);
}

stringstream GLADDB append_entry_to_table_wrapper(stringstream tablestream, vector<EntryValue> values) {
	vector<pair<string, ColumnType>> _converted_values;
	for (const auto value : values) {
		_converted_values.push_back(::convert_entry_value(value));
	}
	return append_entry_to_table(std::stringstream{ tablestream.str(), cast_enum(OpenMode::APPEND) }, _converted_values);
}

optional<stringstream> GLADDB remove_entry_from_table_wrapper(stringstream tablestream, vector<EntryValue> values) {
	vector<string> _converted_values;
	for (const auto value : values) {
		_converted_values.push_back(::convert_entry_value(value).first);
	}
	return remove_entry_from_table(std::stringstream{ tablestream.str(), cast_enum(OpenMode::APPEND) }, _converted_values);
}