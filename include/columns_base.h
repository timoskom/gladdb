#pragma once
#include "config.h"

GLADDBBEGIN

enum class ColumnType {
	Integer,
	Real,
	Text
};

enum class ColumnNullability : ubyte_t {
	Nullable,
	NotNull
};

using ColumnPropertiesType = ubyte_t;

GLADDBINLINE
ColumnPropertiesType create_column_properties(ColumnNullability nullability) {
	return (ColumnPropertiesType)nullability;
}

GLADDBEND