#pragma once
#include "config.h"
#include "columns_base.h"

GLADDBBEGIN

class EntryValue {
public:

	GLADDBINLINE
		EntryValue(const std::string& value, const ColumnType& type)
		:_value{ value }, _type{ type }
	{}

	EntryValue(const EntryValue&) = default;

	GLADDBINLINE virtual ~EntryValue() { }

	EntryValue& operator=(const EntryValue&) = default;

	GLADDBINLINE
		std::string value() const {
		return this->_value;
	}

	GLADDBINLINE
		ColumnType type() const {
		return this->_type;
	}

private:

	std::string _value;
	ColumnType _type;

};

GLADDBEND