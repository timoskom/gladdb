#pragma once

#include "config.h"
#include "columns_base.h"

GLADDBBEGIN

class ColumnInfo {
public:

	GLADDBINLINE
		ColumnInfo(const ColumnType& type, const ColumnPropertiesType& properties)
		:_type{ type }, _properties{ properties }
	{}

	ColumnInfo(const ColumnInfo&) = default;

	GLADDBINLINE virtual ~ColumnInfo() { }

	ColumnInfo& operator=(const ColumnInfo&) = default;

	GLADDBINLINE
		ColumnType type() const {
		return this->_type;
	}

	GLADDBINLINE
		ColumnPropertiesType properties() const {
		return this->_properties;
	}

private:

	ColumnType _type;
	ColumnPropertiesType _properties;

};

GLADDBEND