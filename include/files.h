#pragma once

#include "config.h"
#include "columns_base.h"

GLADDBBEGIN

#ifdef __GLADDB_AC_64__
static const std::string LIB7Z = "x64/7z.dll";
static const std::string LIB7ZA = "x64/7za.dll";
static const std::string LIB7ZRA = "x64/7zra.dll";
static const std::string LIB7ZXA = "x64/7zxa.dll";
static const std::string LIB7ZRR = "x64/7zxr.dll";
#elif defined(__GLADDB_AC_32__)
static const std::string LIB7Z = "x86/7z.dll";
static const std::string LIB7ZA = "x86/7za.dll";
static const std::string LIB7ZRA = "x86/7zra.dll";
static const std::string LIB7ZXA = "x86/7zxa.dll";
static const std::string LIB7ZRR = "x86/7zxr.dll";
#endif

enum class OpenMode : std::ios_base::openmode {
	APPEND = std::ios_base::in | std::ios_base::out | std::ios_base::app,
	TRUNCATE = std::ios_base::in | std::ios_base::out | std::ios_base::trunc
};

GLADDBEND

namespace std {

	GLADDBINLINE
	string to_string(GLADDB ColumnType value) {
		switch (value) {
		case GLADDB ColumnType::Integer:
			return _GLADDB_STRINGIFY(Integer);
		case GLADDB ColumnType::Real:
			return _GLADDB_STRINGIFY(Real);
		case GLADDB ColumnType::Text:
			return _GLADDB_STRINGIFY(Text);
		default:
			return "";
		}
	}

	GLADDBINLINE
	wstring to_wstring(GLADDB ColumnType value) {
		switch (value) {
		case GLADDB ColumnType::Integer:
			return _GLADDB_WSTRINGIFY(Integer);
		case GLADDB ColumnType::Real:
			return _GLADDB_WSTRINGIFY(Real);
		case GLADDB ColumnType::Text:
			return _GLADDB_WSTRINGIFY(Text);
		default:
			return L"";
		}
	}

	GLADDBINLINE
	u8string to_u8string(GLADDB ColumnType value) {
		switch (value) {
		case GLADDB ColumnType::Integer:
			return _GLADDB_U8STRINGIFY(Integer);
		case GLADDB ColumnType::Real:
			return _GLADDB_U8STRINGIFY(Real);
		case GLADDB ColumnType::Text:
			return _GLADDB_U8STRINGIFY(Text);
		default:
			return u8"";
		}
	}

	GLADDBINLINE
	u16string to_u16string(GLADDB ColumnType value) {
		switch (value) {
		case GLADDB ColumnType::Integer:
			return _GLADDB_U16STRINGIFY(Integer);
		case GLADDB ColumnType::Real:
			return _GLADDB_U16STRINGIFY(Real);
		case GLADDB ColumnType::Text:
			return _GLADDB_U16STRINGIFY(Text);
		default:
			return u"";
		}
	}

	GLADDBINLINE
	u32string to_u32string(GLADDB ColumnType value) {
		switch (value) {
		case GLADDB ColumnType::Integer:
			return _GLADDB_U32STRINGIFY(Integer);
		case GLADDB ColumnType::Real:
			return _GLADDB_U32STRINGIFY(Real);
		case GLADDB ColumnType::Text:
			return _GLADDB_U32STRINGIFY(Text);
		default:
			return U"";
		}
	}

	GLADDBINLINE
	string to_string(GLADDB ColumnNullability value) {
		switch (value) {
		case GLADDB ColumnNullability::NotNull:
			return _GLADDB_STRINGIFY(NotNull);
		case GLADDB ColumnNullability::Nullable:
			return _GLADDB_STRINGIFY(Nullable);
		}
	}

	GLADDBINLINE
	wstring to_wstring(GLADDB ColumnNullability value) {
		switch (value) {
		case GLADDB ColumnNullability::NotNull:
			return _GLADDB_WSTRINGIFY(NotNull);
		case GLADDB ColumnNullability::Nullable:
			return _GLADDB_WSTRINGIFY(Nullable);
		}
	}

	GLADDBINLINE
	u8string to_u8string(GLADDB ColumnNullability value) {
		switch (value) {
		case GLADDB ColumnNullability::NotNull:
			return _GLADDB_U8STRINGIFY(NotNull);
		case GLADDB ColumnNullability::Nullable:
			return _GLADDB_U8STRINGIFY(Nullable);
		}
	}

	GLADDBINLINE
	u16string to_u16string(GLADDB ColumnNullability value) {
		switch (value) {
		case GLADDB ColumnNullability::NotNull:
			return _GLADDB_U16STRINGIFY(NotNull);
		case GLADDB ColumnNullability::Nullable:
			return _GLADDB_U16STRINGIFY(Nullable);
		}
	}

	GLADDBINLINE
	u32string to_u32string(GLADDB ColumnNullability value) {
		switch (value) {
		case GLADDB ColumnNullability::NotNull:
			return _GLADDB_U32STRINGIFY(NotNull);
		case GLADDB ColumnNullability::Nullable:
			return _GLADDB_U32STRINGIFY(Nullable);
		}
	}

}

GLADDBBEGIN

template<class CharT, class Traits = std::char_traits<CharT>>
GLADDBINLINE
std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, ColumnType ct) {
	if constexpr (std::is_same<CharT, char>::value) {
		os << std::to_string(ct);
	}
	else if constexpr (std::is_same<CharT, wchar_t>::value) {
		os << std::to_wstring(ct);
	}
	else if constexpr (std::is_same<CharT, char8_t>::value) {
		os << std::to_u8string(ct);
	}
	else if constexpr (std::is_same<CharT, char16_t>::value) {
		os << std::to_u16string(ct);
	}
	else if constexpr (std::is_same<CharT, char32_t>::value) {
		os << std::to_u32string(ct);
	}
	return os;
}

template<class CharT, class Traits = std::char_traits<CharT>>
GLADDBINLINE
std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, ColumnNullability cn) {
	if constexpr (std::is_same<CharT, char>::value) {
		os << std::to_string(cn);
	}
	else if constexpr (std::is_same<CharT, wchar_t>::value) {
		os << std::to_wstring(cn);
	}
	else if constexpr (std::is_same<CharT, char8_t>::value) {
		os << std::to_u8string(cn);
	}
	else if constexpr (std::is_same<CharT, char16_t>::value) {
		os << std::to_u16string(cn);
	}
	else if constexpr (std::is_same<CharT, char32_t>::value) {
		os << std::to_u32string(cn);
	}
	return os;
}

std::optional<std::fstream> write_to_file(const std::string name, const std::string data, const OpenMode mode);

std::stringstream create_table(std::string name, std::string db, std::vector<std::pair<std::string, std::pair<ColumnType, ColumnPropertiesType>>> columns);

std::stringstream append_entry_to_table(std::stringstream tablestream, std::vector<std::pair<std::string, ColumnType>> values);

std::optional<std::stringstream> remove_entry_from_table(std::stringstream tablestream, std::vector<std::string> values);

std::optional<std::fstream> create_master_table_file(std::string db);

std::optional<bool> create_database_file(std::string db);

bool destroy_database_file(std::string db);

std::optional<tfstream> get_database_file(std::string db, bool binary_mode);

GLADDBEND