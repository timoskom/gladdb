#include "include/Table.h"
#include "include/files_table_ext.h"

using std::fstream;
using std::string;
using std::vector;

GLADDB Table::Table(const string& name, const string& db, vector<Column> columns)
	:_stream{}, _name{ name }, _db{ db }, _columns{}, _entries{}
{}

bool GLADDB Table::create() {
	std::stringstream strstream = create_table_wrapper(this->_name, this->_db, this->_columns);
	this->_stream.open(this->_name, cast_enum(OpenMode::TRUNCATE));
	if (!(this->_stream.fail() || this->_stream.bad())) {
		this->_stream << strstream.str();
		return true;
	}
	else {
		return false;
	}
}

bool GLADDB Table::appendEntry(Entry values) {
	std::stringstream strstream{ cast_enum(OpenMode::APPEND) };
	strstream << this->_stream.rdbuf();
	std::stringstream resultstream = append_entry_to_table_wrapper(std::stringstream{ strstream.str(), cast_enum(OpenMode::APPEND) }, values);
	if (this->_stream.is_open()) {
		this->_stream.close();
	}
	this->_stream.open(this->_db + ".txt", cast_enum(OpenMode::TRUNCATE));
	if (!(this->_stream.fail() || this->_stream.bad())) {
		this->_stream << resultstream.str();
		this->_entries.push_back(values);
		return true;
	}
	else {
		return false;
	}
}

bool GLADDB Table::removeEntry(Entry values) {
	std::stringstream strstream{ cast_enum(OpenMode::APPEND) };
	strstream << this->_stream.rdbuf();
	auto result = remove_entry_from_table_wrapper(std::stringstream{ strstream.str(), cast_enum(OpenMode::APPEND) }, values);
	if (result.has_value()) {
		if (this->_stream.is_open()) {
			this->_stream.close();
		}
		this->_stream.open(this->_db + ".txt", cast_enum(OpenMode::TRUNCATE));
		if (!(this->_stream.fail() || this->_stream.bad())) {
			this->_stream << result.value().str();
			return true;
		}
		else {
			return false;
		}
	} else {
		return false;
	}
}