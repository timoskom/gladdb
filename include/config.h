#pragma once

#include <cstddef>
#include <cstdint>
#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <optional>
#include <bit7z/bitarchivereader.hpp>
#include <bit7z/bitarchivewriter.hpp>
#include <bit7z/bitarchiveeditor.hpp>
#include <bit7z/bitfilecompressor.hpp>
#include <bit7z/bitfileextractor.hpp>
#include <bit7z/bitmemcompressor.hpp>
#include <bit7z/bitmemextractor.hpp>
#include <bit7z/bitstreamcompressor.hpp>
#include <bit7z/bitstreamextractor.hpp>

#if defined(_M_AMD64) || defined(__x86_64) || defined(__x86_64__) || defined(__amd64) || defined(__amd64__) || defined(_M_X64)
/*
 x86-64 Architecture
*/
# define __GLADDB_AC_64__ 1
#elif defined(_M_IX86) || defined(i386) || defined(__i386) || defined(__i386__) || defined(__IA23__) || defined(__X86__) || defined(_X86_) || defined(__I86__) || defined(__THW_INTEL__) || defined(__INTEL__) || defined(__386)
/*
 x86 Architecture
*/
# define __GLADDB_AC_32__ 1
#elif defined(__arm__) || defined(__thumb__) || defined(__ARM_ARCH_ARM) || defined(__TARGET_ARCH_THUMB) || defined(_ARM) || defined(_M_ARM) || defined(_M_ARMT) || defined(__arm)
/*
 ARM Architecture
*/
# define __GLADDB_AC_ARM__ 1
#elif defined(__aarch64__)
/*
 ARM64 Architecture
*/
# define __GLADDB_AC_ARM64__ 1
#else
# error Unsupported architecture
#endif

#if defined(__GLADDB_AC_ARM__) || defined(__GLADDB_AC_ARM64__)
# error ARM architectures are not yet supported
#endif

#ifndef _GLADDB
# define _GLADDB
#else
# error _GLADDB is already defined
#endif // !_GLADDB

#ifndef _GLADDB_NAME
# define _GLADDB_NAME "gladDB"
#else
# error _GLADDB_NAME is already defined
#endif // !_GLADDB_NAME

#ifndef _GLADDB_VERSION_NUM
# define _GLADDB_VERSION_NUM 1
#else
# error _GLADDB_VERSION_NUM is already defined
#endif // !_GLADDB_VERSION_NUM

#ifndef _GLADDB_VERSION_STR
# define _GLADDB_VERSION_STR "0.0.1"
#else
# error _GLADDB_VERSION_STR is already defined
#endif // !_GLADDB_VERSION_STR

#ifndef _GLADDB_NAMESPACE
# define _GLADDB_NAMESPACE gladdb
#else
# error _GLADDB_NAMESPACE is already defined
# endif // !_GLADDB_NAMESPACE

#ifndef _GLADDB_NAMESPACE_BEGIN
# define _GLADDB_NAMESPACE_BEGIN namespace _GLADDB_NAMESPACE {
#else
# error _GLADDB_NAMESPACE_BEGIN is already defined
# endif // !_GLADDB_NAMESPACE_BEGIN

#ifndef _GLADDB_NAMESPACE_END
# define _GLADDB_NAMESPACE_END }
#else
# error _GLADDB_NAMESPACE_END is already defined
# endif // !_GLADDB_NAMESPACE_END

#ifndef _GLADDB_INLINE
# define _GLADDB_INLINE inline
#else
# error _GLADDB_INLINE is already defined
#endif // !_GLADDB_INLINE

#ifndef _GLADDB_STRINGIFY
# define _GLADDB_STRINGIFY(__V__) #__V__
#else
# error _GLADDB_STRINGIFY is already defined
#endif // !_GLADDB_STRINGIFY

#ifndef _GLADDB_WSTRINGIFY
# define _GLADDB_WSTRINGIFY(__V__) L#__V__
#else
# error _GLADDB_WSTRINGIFY is already defined
#endif // !_GLADDB_WSTRINGIFY

#ifndef _GLADDB_U8STRINGIFY
# define _GLADDB_U8STRINGIFY(__V__) u8#__V__
#else
# error _GLADDB_U8STRINGIFY is already defined
#endif // !_GLADDB_U8STRINGIFY

#ifndef _GLADDB_U16STRINGIFY
# define _GLADDB_U16STRINGIFY(__V__) u#__V__
#else
# error _GLADDB_U16STRINGIFY is already defined
#endif // !_GLADDB_U16STRINGIFY

#ifndef _GLADDB_U32STRINGIFY
# define _GLADDB_U32STRINGIFY(__V__) U#__V__
#else
# error _GLADDB_U32STRINGIFY is already defined
#endif // !_GLADDB_U32STRINGIFY

#define GLADDB _GLADDB_NAMESPACE ::

#define GLADDBBEGIN _GLADDB_NAMESPACE_BEGIN

#define GLADDBEND _GLADDB_NAMESPACE_END

#define GLADDBINLINE _GLADDB_INLINE

GLADDBBEGIN

using schar  = signed char       ;
using uchar  = unsigned char     ;
using ushort = unsigned short    ;
using uint   = unsigned int      ;
using ulong  = unsigned long     ;
using ullong = unsigned long long;
using char8  = char8_t           ;
using char16 = char16_t          ;
using char32 = char32_t          ;
using utf8   = char8_t           ;
using utf16  = char16_t          ;
using utf32  = char32_t          ;

using i8  = int8_t  ;
using i16 = int16_t ;
using i32 = int32_t ;
using i64 = int64_t ;
using u8  = uint8_t ;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using fast8   = int_fast8_t  ;
using fast16  = int_fast16_t ;
using fast32  = int_fast32_t ;
using fast64  = int_fast64_t ;
using ufast8  = uint_fast8_t ;
using ufast16 = uint_fast16_t;
using ufast32 = uint_fast32_t;
using ufast64 = uint_fast64_t;

using least8   = int_least8_t  ;
using least16  = int_least16_t ;
using least32  = int_least32_t ;
using least64  = int_least64_t ;
using uleast8  = uint_least8_t ;
using uleast16 = uint_least16_t;
using uleast32 = uint_least32_t;
using uleast64 = uint_least64_t;

using iptr = intptr_t ;
using uptr = uintptr_t;

using imax = intmax_t ;
using umax = uintmax_t;

using byte_t   = i8 ;
using word_t   = i16;
using dword_t  = i32;
using qword_t  = i64;
using ubyte_t  = u8 ;
using uword_t  = u16;
using udword_t = u32;
using uqword_t = u64;

template<typename _Type> using value_t          = _Type       ;
template<typename _Type> using pointer_t        = _Type*      ;
template<typename _Type> using const_pointer_t  = const _Type*;
template<typename _Type> using iterator_t       = _Type*      ;
template<typename _Type> using const_iterator_t = const _Type*;

using tstring = std::u16string;
using tfstream = std::basic_fstream<utf16>;

GLADDBEND

namespace std {

	using u8fstream = basic_fstream<char8_t>;
	using u16fstream = basic_fstream<char16_t>;
	using u32fstream = basic_fstream<char32_t>;

	template<class CharT, class Traits = std::char_traits<CharT>, class Allocator = std::allocator<CharT>>
	void writeline(std::basic_ostream<CharT, Traits>& _ostream, std::basic_string<CharT, Traits, Allocator> _string) {
		_ostream << _string << std::endl;
	}

	template<class CharT, class Traits = std::char_traits<CharT>, class Allocator = std::allocator<CharT>>
	void writeline(std::basic_ostream<CharT, Traits>& _ostream, const CharT* _string, const Allocator& alloc = Allocator()) {
		_ostream << _string << std::endl;
	}

	template<class CharT, class Traits = std::char_traits<CharT>, class Allocator = std::allocator<CharT>>
	void writeline(std::basic_ostream<CharT, Traits>& _ostream, std::initializer_list<CharT> ilist, const Allocator& alloc = Allocator()) {
		_ostream << ilist << std::endl;
	}

}


template <typename E, typename = typename std::enable_if_t<std::is_enum_v<E>>>
GLADDBINLINE
constexpr typename std::underlying_type_t<E> cast_enum(E e) noexcept {
	return static_cast<typename std::underlying_type_t<E>>(e);
}